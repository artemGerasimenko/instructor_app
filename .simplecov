SimpleCov.start 'rails' do
  add_filter do |source_file|
    source_file.lines.count < 2
  end
end
SimpleCov.minimum_coverage 0
SimpleCov.maximum_coverage_drop 100
