Rails.application.routes.draw do
  resources :admins
  resources :workers
  resources :departaments
  resources :buckets
  resources :superadmins

  get "/bucket", to: "buckets#new"
  post "/bucket", to: "buckets#create"
  get "/departament", to:"departaments#index"
  get "/signup",   to:"workers#new"
  post '/signup',  to:"workers#create"
  get "/workers", to: "admins#workers"


  resources :sessions, only: [:new, :create, :destroy]
  get "/", to:"sessions#new"
  post "/session", to:"sessions#create"

  delete "/signout", to:"sessions#destroy"

  delete "/buckets", to:"buckets#destroy"


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
