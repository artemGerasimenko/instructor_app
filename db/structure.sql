--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE admins (
    id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    age integer NOT NULL,
    phone character varying,
    departament_id integer,
    remember_token character varying,
    password_digest character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: admins_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE admins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE admins_id_seq OWNED BY admins.id;


--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: buckets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE buckets (
    id integer NOT NULL,
    worker_id integer,
    part_of_day boolean NOT NULL,
    day character varying NOT NULL,
    departament_id integer,
    is_saved boolean DEFAULT false,
    is_chosen_on_current_week boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: buckets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE buckets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: buckets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE buckets_id_seq OWNED BY buckets.id;


--
-- Name: departaments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE departaments (
    id integer NOT NULL,
    name character varying NOT NULL,
    if_schedule_on_current_week_is_approve boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: departaments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE departaments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: departaments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE departaments_id_seq OWNED BY departaments.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: superadmins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE superadmins (
    id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    age integer NOT NULL,
    phone character varying,
    remember_token character varying,
    password_digest character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: superadmins_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE superadmins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: superadmins_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE superadmins_id_seq OWNED BY superadmins.id;


--
-- Name: workers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE workers (
    id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    age integer NOT NULL,
    status boolean,
    phone character varying,
    hours integer DEFAULT 0,
    departament_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    password_digest character varying,
    remember_token character varying
);


--
-- Name: workers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE workers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: workers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE workers_id_seq OWNED BY workers.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY admins ALTER COLUMN id SET DEFAULT nextval('admins_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY buckets ALTER COLUMN id SET DEFAULT nextval('buckets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY departaments ALTER COLUMN id SET DEFAULT nextval('departaments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY superadmins ALTER COLUMN id SET DEFAULT nextval('superadmins_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY workers ALTER COLUMN id SET DEFAULT nextval('workers_id_seq'::regclass);


--
-- Name: admins_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY admins
    ADD CONSTRAINT admins_pkey PRIMARY KEY (id);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: buckets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY buckets
    ADD CONSTRAINT buckets_pkey PRIMARY KEY (id);


--
-- Name: departaments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY departaments
    ADD CONSTRAINT departaments_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: superadmins_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY superadmins
    ADD CONSTRAINT superadmins_pkey PRIMARY KEY (id);


--
-- Name: workers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY workers
    ADD CONSTRAINT workers_pkey PRIMARY KEY (id);


--
-- Name: index_admins_on_departament_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_admins_on_departament_id ON admins USING btree (departament_id);


--
-- Name: index_admins_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_admins_on_email ON admins USING btree (email);


--
-- Name: index_buckets_on_worker_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_buckets_on_worker_id ON buckets USING btree (worker_id);


--
-- Name: index_departaments_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_departaments_on_name ON departaments USING btree (name);


--
-- Name: index_workers_on_departament_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_workers_on_departament_id ON workers USING btree (departament_id);


--
-- Name: index_workers_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_workers_on_email ON workers USING btree (email);


--
-- Name: index_workers_on_remember_token; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_workers_on_remember_token ON workers USING btree (remember_token);


--
-- Name: fk_rails_92f2fb8ea1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY workers
    ADD CONSTRAINT fk_rails_92f2fb8ea1 FOREIGN KEY (departament_id) REFERENCES departaments(id);


--
-- Name: fk_rails_a07f3e685d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY admins
    ADD CONSTRAINT fk_rails_a07f3e685d FOREIGN KEY (departament_id) REFERENCES departaments(id);


--
-- Name: fk_rails_f2a6e41d81; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY buckets
    ADD CONSTRAINT fk_rails_f2a6e41d81 FOREIGN KEY (worker_id) REFERENCES workers(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20161118212238'), ('20161118214613'), ('20161201000246'), ('20161201000638'), ('20161202170241'), ('20161204173320'), ('20161216145232');


