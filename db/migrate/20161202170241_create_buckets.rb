class CreateBuckets < ActiveRecord::Migration[5.0]
  def change
    create_table :buckets do |t|
      t.integer :worker_id
      t.boolean :part_of_day, null:false
      t.string :day ,null:false
      t.integer :departament_id
      t.boolean :is_saved , default: false
      t.boolean :is_chosen_on_current_week, default:false
      t.belongs_to :worker, index:true, foreign_key: true
      t.timestamps
    end
  end
end
