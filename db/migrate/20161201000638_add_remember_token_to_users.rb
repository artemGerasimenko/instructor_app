class AddRememberTokenToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :workers, :remember_token, :string
    add_index :workers, :remember_token
  end
end
