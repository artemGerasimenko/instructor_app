class CreateDepartaments < ActiveRecord::Migration[5.0]
  def change
    create_table :departaments do |t|
      t.string :name, null:false
      t.boolean :if_schedule_on_current_week_is_approve ,
                default: false
      t.timestamps
    end
    add_index :departaments, :name, unique: true
  end
end
