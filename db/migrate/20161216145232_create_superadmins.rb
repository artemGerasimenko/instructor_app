class CreateSuperadmins < ActiveRecord::Migration[5.0]
  def change
    create_table :superadmins do |t|
      t.string :name ,null:false
      t.string :email, null:false
      t.integer :age, null:false
      t.string :phone
      t.string :remember_token
      t.string :password_digest
      t.timestamps
      t.timestamps
    end
  end
end
