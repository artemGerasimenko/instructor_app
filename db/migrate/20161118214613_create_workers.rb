class CreateWorkers < ActiveRecord::Migration[5.0]
  def change
    create_table :workers do |t|
      t.string :name, null:false
      t.string :email, null:false
      t.integer :age, null:false
      t.boolean :status
      t.string :phone
      t.integer :hours , default: 0
      t.integer :departament_id
      t.belongs_to :departament, index: true, foreign_key: true
      t.timestamps
    end
    add_index :workers, :email, unique:true
  end
end
