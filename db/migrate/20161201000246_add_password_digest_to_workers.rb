class AddPasswordDigestToWorkers < ActiveRecord::Migration[5.0]
  def change
    add_column :workers, :password_digest, :string
  end
end
