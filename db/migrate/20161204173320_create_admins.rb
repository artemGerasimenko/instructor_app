class CreateAdmins < ActiveRecord::Migration[5.0]
  def change
    create_table :admins do |t|
      t.string :name, null:false
      t.string :email, null:false
      t.integer :age, null:false
      t.string :phone
      t.integer :departament_id
      t.string :remember_token
      t.string :password_digest
      t.belongs_to :departament, index: true , foreign_key: true
      t.timestamps
    end
    add_index :admins, :email, unique:true
  end
end
