class Departament < ApplicationRecord
   has_many :worker
   has_one :admin
   after_destroy :delete_workers_and_admins

   validates(:name, presence: true, length: {maximum: 50})


   def Departament.make_unapproved
      Departament.update_all(if_schedule_on_current_week_is_approve:false)
   end

   def delete_workers_and_admins
   	self.worker.destroy_all if  self.worker
   	self.admin.destroy if self.admin
   end

end
