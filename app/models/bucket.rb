class Bucket < ApplicationRecord
  belongs_to :worker

  def self.delete_old_enrolls
    Bucket.all.each do |bu|
      bu.destroy if bu.created_at<Time.now.beginning_of_week-7.day
    end
  end

  def self.update_hours(part_of_day)
    hash = Hash.new
    hash[1]="Monday"
    hash[2]="Tuesday"
    hash[3]="Wednesday"
    hash[4]="Thursday"
    hash[5]="Friday"
    hash[6]="Saturday"
    hash[0]="Sunday"

    Bucket.all.each do |bu|
      if bu.created_at>Time.now.beginning_of_week-7.day &&
          bu.created_at<Time.now.end_of_week-7.day &&
           bu.part_of_day.to_s == part_of_day &&
            bu.day==hash[Time.now.wday] &&
             bu.is_chosen_on_current_week == true
        bu.worker.update_attribute(:hours,bu.worker.hours+5)
      end
    end
  end
end
