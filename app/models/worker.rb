class Worker < ApplicationRecord
  belongs_to :departament
  has_many :bucket
  validates(:name, presence: true, length: {maximum: 50})
  VALID_EMAIL_REGES = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates(:email, presence: true, format: {with: VALID_EMAIL_REGES}, uniqueness: {case_sensetive: false})
  validates(:password, length:{minimum: 6})
  has_secure_password
  after_destroy :delete_buckets


  def Worker.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def Worker.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  private

    def create_remember_token
      self.remember_token = Worker.encrypt(Worker.new_remember_token)
    end

    def delete_buckets 
      self.bucket.destroy_all if self.bucket
    end
     

end
