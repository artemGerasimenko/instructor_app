class AdminsController < ApplicationController
    before_action :set_worker, only: [:show, :edit, :update, :destroy]
    before_action :signed_in_worker, only: [:edit, :update, :index]
    before_action :correct_worker,   only: [:edit, :update]
    before_action :redirect, only: [:index]

    def workers
      @workers = Worker.where("departament_id = #{current_worker.departament_id}")
    end

    def redirect
      if cookies[:admin] == "false"
        redirect_to current_worker
      end
    end


    def show
      @worker = Admin.find(params[:id])
    end

    def edit
      @worker = current_worker
    end

    def index
      @timetable = Bucket.first
      @admin = current_worker
    end

    def update
      if params["commit"] == "Блокировать"
        current_worker.departament.update_attribute(:if_schedule_on_current_week_is_approve, true)
        redirect_to '/admins'
      elsif params["commit"] == "Разблокировать"
        current_worker.departament.update_attribute(:if_schedule_on_current_week_is_approve, false)
        redirect_to '/admins'
      else
        worker = Admin.find(params[:id])
        if  worker && worker.authenticate(params[:admin][:password])
          if @worker.update_attributes(worker_params)
            render 'edit'
          else
            render 'edit'
          end
        else
          render 'edit'
        end
      end
    end


    private
      def set_worker
        @worker = Admin.find(params[:id])
      end

      def worker_params
        params.require(:admin).permit(:name, :email, :age, :status, :phone, :hours, :departament_id, :password, :password_conformation)
      end

      def signed_in_worker
        redirect_to '/', notice: "Please sign in." unless signed_in?
      end

      def correct_worker
        @worker = Admin.find(params[:id])
        redirect_to @worker unless current_worker?(@worker)
      end
end
