class DepartamentsController < ApplicationController
  before_action :set_departament, only: [:show, :edit, :update, :destroy]
  before_action :is_superadmin, only: [:show, :edit, :update, :destroy, :index]

  def is_superadmin
    if cookies[:superadmin] == "true"
    else
      if current_worker
        redirect_to current_worker
      else
        redirect_to '/'
      end
    end
  end

  def index
    @departaments = Departament.all
  end

  def show
  end

  def new
    @departament = Departament.new(if_schedule_on_current_week_is_approve:false)
  end

  def edit
  end

  def create
    @departament = Departament.new(departament_params)
    respond_to do |format|
      if @departament.save
        format.html { redirect_to @departament, notice: 'Departament was successfully created.' }
        format.json { render :show, status: :created, location: @departament }
      else
        format.html { render :new }
        format.json { render json: @departament.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @departament.update(departament_params)
        format.html { redirect_to @departament, notice: 'Departament was successfully updated.' }
        format.json { render :show, status: :ok, location: @departament }
      else
        format.html { render :edit }
        format.json { render json: @departament.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @departament.destroy
    respond_to do |format|
      format.html { redirect_to departaments_url, notice: 'Departament was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_departament
      @departament = Departament.find(params[:id])
    end

    def departament_params
      params.require(:departament).permit(:name,:if_schedule_on_current_week_is_approve)
    end
end
