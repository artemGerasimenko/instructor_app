class WorkersController < ApplicationController
  before_action :set_worker, only: [:show, :edit, :update, :destroy]
  before_action :signed_in_worker, only: [:edit, :update, :index]
  before_action :correct_worker,   only: [:edit, :update]
  before_action :redirect, only: [:index]



  def index
      @workers = Worker.all
  end



  def redirect
    if cookies[:admin] == "false"
      redirect_to current_worker
    end
  end

  def show
    if cookies[:admin] == "true"
      worker = Worker.find(params[:id])
    else
      @worker = Worker.find(current_worker.id)
    end

  end


  def new
    @worker = Worker.new
  end


  def edit
    @worker = Worker.find(params[:id])
  end


  def create
    if params["worker"]["status"] == "Admin"
      @worker = Admin.new(admin_params)
    else
      @worker = Worker.new(worker_params)
      end
    if @worker.save
      cookies.permanent[:superadmin] = false
      if params["worker"]["status"] == "Admin"
        cookies.permanent[:admin] = true
        cookies.permanent[:superadmin] = false
      else
        cookies.permanent[:admin] = false
        cookies.permanent[:superadmin] = false
      end
      if params["worker"]["status"] == "Admin"
        sign_in @worker
        redirect_to '/admins'
      else
        sign_in @worker
        redirect_to @worker
      end

    else
      render 'new'
    end
  end


  def update
    worker = Worker.find(params[:id])
    if  worker && worker.authenticate(params[:worker][:password])
      if @worker.update_attributes(worker_params)
        flash[:success] = "Profile update"
        redirect_to edit_worker_path
      else
        flash.now[:danger] = "Server error"
        render 'edit'
      end
    else
      flash.now[:danger] = "Invalid password"
      render 'edit'
    end
  end


  def destroy
    @worker.destroy
    respond_to do |format|
      format.html { redirect_to workers_url, notice: 'Работник удален' }
      format.json { head :no_content }
    end
  end

  private

    def set_worker
      @worker = Worker.find(params[:id])
    end

    def worker_params
      params.require(:worker).permit( :name, :email, :age, :status, :phone, :hours, :departament_id, :password, :password_conformation)
    end

    def admin_params
      params.require(:worker).permit( :name, :email, :age, :phone, :departament_id, :password,     :password_conformation)
    end

  def signed_in_worker
    redirect_to '/', notice: "Please sign in." unless signed_in?
  end

  def correct_worker
      @worker = Worker.find(params[:id])
      redirect_to @worker unless current_worker?(@worker)
  end
end
