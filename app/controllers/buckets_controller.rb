class BucketsController < ApplicationController

  before_action :redirect, only: [:new]

  def show
    @bucket = Bucket.find(params[:id])

  end

  def redirect
    if cookies[:admin] == "true" || cookies[:superadmin] == "true"
      redirect_to '/admins'
    elsif
      if current_worker.nil?
        redirect_to '/'
      end
    end
  end


  def new
    @bucket = Bucket.new

  end


  def update
    current_worker.departament.update_attribute(:if_schedule_on_current_week_is_approve,true)
    days_array = ["Monday0", "Tuesday0", "Wednesday0", "Thursday0", "Friday0", "Saturday0", "Sunday0", "Monday1", "Tuesday1", "Wednesday1", "Thursday1", "Friday1", "Saturday1", "Sunday1"]
    i = 0
    while i < 14 do
      if i < 7
        part_of_day = 'f'
      else
        part_of_day = 't'
      end

      if params["#{days_array[i]}"] and params["#{days_array[i]}"] != ""
        Bucket.where("day = '#{days_array[i][0..(days_array[i].length-2)]}' and part_of_day = '#{part_of_day}' and departament_id = #{current_worker.departament.id} and created_at > '#{Time.now.beginning_of_week}'").map{|x| x.update_attribute(:is_chosen_on_current_week, false)}
        Bucket.where("day = '#{days_array[i][0..(days_array[i].length-2)]}' and part_of_day = '#{part_of_day}' and created_at > '#{Time.now.beginning_of_week}' and worker_id = #{Worker.find_by(email: params["#{days_array[i]}"]).id} ").map{|x| x.update_attribute(:is_chosen_on_current_week, true)}
      end
      i = i + 1
    end
    flash.now[:success] = "Данные сохранены"
    redirect_to '/admins'
  end


  def create
  #deleting old  enrolls with current_worker and adding new enrolls on current week



   
    Bucket.where(worker_id: current_worker.id).each do |bucket|
      if (bucket.created_at>Time.now.beginning_of_week)

          bucket.destroy!

      end
    end

    days=["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]

    days.each do |day|
      if params[day.downcase+"0"]
        Bucket.create(worker_id: current_worker.id,day: day, part_of_day: false,
                      departament_id: current_worker.departament.id)
      end
    end

    days.each do |day|
      if params[day.downcase+"1"]
        Bucket.create(worker_id: current_worker.id,day: day, part_of_day: true,
                      departament_id: current_worker.departament.id)
      end
    end
    redirect_to '/bucket'
  end

  # def destroy
  #   @bucket=Bucket.find_by(params[:id])
  #   @bucket.destroy
  #   redirect_to new_bucket_path
  #   @bucket = Bucket.new
  #
  # end

  private

  def bucket_params
    #debugger
    params.permit(
            :worker_id,
            :part_of_day,
            :day,
            :departament_id,
            :is_saved,
            :is_chosen_on_current_week
        )
  end
end
