class SessionsController < ApplicationController
  before_action :redirect, only: [:new]


  def redirect
    if !current_worker.nil?
       redirect_to current_worker
    end
  end

  def create
    worker = Worker.find_by(email: params[:session][:email])
    admin = Admin.find_by(email: params[:session][:email])
    superadmin = Superadmin.find_by(email: params[:session][:email])
    if worker && worker.authenticate(params[:session][:password])
      sign_in worker
      cookies.permanent[:superadmin] = false
      cookies.permanent[:admin] = false
      redirect_back_or worker
    elsif admin && admin.authenticate(params[:session][:password])
       sign_in admin
       cookies.permanent[:superadmin] = false
       cookies.permanent[:admin] = true
       sign_in admin
       redirect_to '/admins'
    elsif superadmin && superadmin.authenticate(params[:session][:password])
       sign_in superadmin
       cookies.permanent[:superadmin] = true
       cookies.permanent[:admin] = false
       sign_in superadmin
       redirect_to '/superadmins'
    else
       flash.now[:danger] = "Некорректные данные!!!"
       render 'new'
    end
  end


  def destroy
    sign_out
    redirect_to '/'
  end
end
