module BucketsHelper
  def find_bucket(id,day,part_of_day)
    bucket=Bucket.where(day: day,part_of_day: part_of_day,worker_id: id).take
  end

  def find_bucket_on_next_week(worker_id, day, part_of_day, departament_id)
    bucket=Bucket.where(day: day,part_of_day: part_of_day,worker_id: worker_id,departament_id:departament_id)
    if(!bucket.empty?)
      bucket.each do |bu|
        if bu.created_at>Time.now.beginning_of_week
          return bu
        end
      end
      return nil
    else
      nil
    end
  end

  def find_bucket_on_current_week(id, day, part_of_day, departament_id)
    bucket=Bucket.where(day: day,part_of_day: part_of_day,worker_id: id,departament_id:departament_id)
    if(!bucket.empty?)
      bucket.each do |bu|
        if bu.created_at<Time.now.beginning_of_week && bu.created_at>(Time.now-7.days).beginning_of_week
          return bu
        end
      end
      return nil
    else
      nil
    end
  end

 # if there is enroll with this data in Bucket
 #  def if_already_checked(id,day,part_of_day)
 #    Bucket.where(worker_id: id,day: day,part_of_day: part_of_day ).take
 #  end


# this methods views status of enrolls in each cell in table
  def viewing_status_of_shecdule_on_the_next_week(worker_id,day,part_of_day,pseudonym_for_day,departament_id)
    @_worker_id=worker_id
    @_day=day
    @_part_of_day=part_of_day
    @_pseudonym_for_day=pseudonym_for_day
    @_departament_id=departament_id

    render 'view_of_table_for_next_week'
  end

  def viewing_status_of_shecdule_on_current_week(worker_id,day,part_of_day,pseudonym_for_day,departament_id)
    @_worker_id=worker_id
    @_day=day
    @_part_of_day=part_of_day
    @_pseudonym_for_day=pseudonym_for_day
    @_departament_id=departament_id

    render 'buckets/view_of_table_for_current_week'
  end



end

