module SessionsHelper
  def sign_in(worker)
    remember_token = Worker.new_remember_token
    cookies.permanent[:remember_token] = remember_token
    worker.update_attribute(:remember_token,(Worker.encrypt(remember_token)))
    self.current_worker = worker
  end

  def current_worker=(worker)
    @current_worker = worker
  end

  def current_worker
    remember_token = Worker.encrypt(cookies[:remember_token])
    if cookies[:admin] == "false" && cookies[:superadmin] == "false"
      @current_worker ||= Worker.find_by(remember_token: remember_token)
    elsif cookies[:admin] == "true"
      @current_worker ||= Admin.find_by(remember_token: remember_token)
    else
      @current_worker ||= Superadmin.find_by(remember_token: remember_token)
    end
    @current_worker ||= Worker.find_by(remember_token: remember_token)
  end

  def current_worker?(worker)
    worker == current_worker
  end



  def signed_in?
    !current_worker.nil?
  end

  def sign_out
    #current_worker.update_attribute(:remember_token,(Worker.encrypt(Worker.new_remember_token)))
    cookies.delete(:remember_token)
    cookies.delete(:admin)
    cookies.delete(:superadmin)
    self.current_worker = nil
  end


  def redirect_back_or(default)
    redirect_to(session[:return_to]||default)
    session.delete(:return_to)
  end


  def store_lokation
    session[:return_to] = request.url if request.get?
  end
end
