require 'rails_helper'
require 'capybara/rails'
require 'capybara/rspec'

feature 'destroing departament' do
  before(:each) do
    @superadmin=FactoryGirl.create(:superadmin,name:'Superadmin',email:'s@s.s',
                                   password:'password',age:22)
  end

  scenario 'superadmin creates and destroys departament' do
    visit '/'
    fill_in 'session_email', :with=>@superadmin.email
    fill_in 'session_password', :with=>@superadmin.password
    click_button 'Вход'
    click_link 'Отделы'
    click_link 'Новый отдел'
    fill_in 'departament_name', :with =>'New departament'
    click_button 'Create Departament'
    expect(Departament.count).to eq 1
  end
end

feature 'is not allowed to create departament without name' do
  before(:each) do
    @superadmin=FactoryGirl.create(:superadmin,name:'Superadmin',email:'s@s.s',
                                   password:'password',age:22)
  end
  scenario 'creating departament without name' do
  visit '/'
  fill_in 'session_email', :with=>@superadmin.email
  fill_in 'session_password', :with=>@superadmin.password
  click_button 'Вход'
  click_link 'Отделы'
  click_link 'Новый отдел'
  fill_in 'departament_name', :with =>''
  click_button 'Create Departament'
  expect(Departament.count).to eq 0

  end
end

#ask Ihor why this decsrease covered percent
#
# describe DepartamentsController do
#    describe 'POST :create' do
#     it 'creates departament and saves it in database' do
#       process :create, method: :post, params: {
#           departament: {
#                 name: 'Departament_1',
#               }
#       }
#       departament = Departament.first
#       expect(departament.name).to eq 'Departament_1'
#     end
#   end
# end
#   # describe 'GET :index' do
#   #   let!(:departament) { create(:departament, name: 'Departament_1') }
#   #   it 'returns all departaments' do
#   #     process :index, method: :get
#   #     #debugger
#   #     expect(assigns(:departaments)).to eq [departament]
#   #   end
#   # end
#
#   # it "renders the index template" do
#   #   process :index, method: :get
#   #   expect(response).to render_template('index')
#   # end
#
#
#   describe 'make departaments unapproved' do
#     let!(:departament) { create(:departament, name: 'Departament_1') }
#     it 'returns value false' do
#       Departament.make_unapproved
#       expect(Departament.last.if_schedule_on_current_week_is_approve).to eq false
#     end
#   end
# end
