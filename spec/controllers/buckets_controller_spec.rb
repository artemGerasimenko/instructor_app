require 'rails_helper'
require 'capybara/rails'
require 'capybara/rspec'




feature "User signup" do

  before(:each) do
    FactoryGirl.create(:departament, name: 'Departament_1')
  end

  scenario "press checkbox on worker's scheudle page"  do
    visit 'signup'
    fill_in 'worker_name', :with => 'Tomm'
    fill_in 'worker_email', :with => 'tomRedl@t.t'
    fill_in 'worker_age', :with => '20'
    fill_in 'worker_password', :with => '123456'
    select 'Departament_1', :from => 'Отдел'
    click_button 'Создать аккаунт'

    click_link 'Расписание'
    check 'monday0'
    click_button 'Подтвердить'
    expect(Bucket.count).to eq 1

    check 'monday0'
    click_button 'Подтвердить'
    expect(Bucket.count).to eq 1

    check 'monday1'
    click_button 'Подтвердить'
    expect(Bucket.count).to eq 2
  end

end

feature "admin choices worker" do
  before(:each) do
    departament = FactoryGirl.create(:departament, name: 'Departament_1')
    @admin = FactoryGirl.create(:admin, name:'Admin', departament_id:departament.id,
                              password:'password',email:'a@a.a',age:22)
    @worker = FactoryGirl.create(:worker, name:'Worker', departament_id:departament.id,
                                password:'password',email:'w@w.w')
    bucket = FactoryGirl.create(:bucket,worker_id: @worker.id,departament_id:departament.id,
                                part_of_day:false,day:'Monday')
  end
#write test then admin will be working correctly
  scenario "admin choices worker for next week , then worker signin"  do
    visit '/'
    fill_in 'session_email', :with => @admin.email
    fill_in 'session_password', :with => @admin.password
    click_button 'Вход'
    click_link 'Расписание'
    expect(@worker.reload.departament.if_schedule_on_current_week_is_approve).not_to eq true
    click_button 'Подтвердить'
    expect(@worker.reload.departament.if_schedule_on_current_week_is_approve).to eq true

    click_button 'Разблокировать'
    expect(@worker.reload.departament.if_schedule_on_current_week_is_approve).not_to eq true

    click_button 'Блокировать'
    expect(@worker.reload.departament.if_schedule_on_current_week_is_approve).to eq true

    click_link 'Выход'

    visit '/'
    fill_in 'session_email', :with => @worker.email
    fill_in 'session_password', :with=> @worker.password
    click_button 'Вход'
    click_link 'Расписание'
    # expect(assign(:id)).to eq 'circle_not_chosen_on_current_week'
    #page.find("#circle_not_chosen_by_admin_on_current_week")

  end

  scenario 'admin edits his name and email' do
    visit '/'
    fill_in 'session_email', :with => @admin.email
    fill_in 'session_password', :with => @admin.password
    click_button 'Вход'
    click_link 'Профиль'

    # 'Invalid password' is not showed
    fill_in 'admin_name', :with=>'Admin Adminovich'
    fill_in 'admin_email', :with=>'admin_new@a.a'
    fill_in 'admin_password', :with=>'wrong password'
    click_button 'Сохранить изменения'
    #expect(page).to have_content 'Invalid password'

  #  faild
    fill_in 'admin_name', :with=>'Admin Adminovich'
    fill_in 'admin_email', :with=>'admin_new@a.a'
    fill_in 'admin_password', :with=>@admin.password
    click_button 'Сохранить изменения'
    #expect(page).to have_content 'Profile update'


    # click_button 'noButton'
  end
end