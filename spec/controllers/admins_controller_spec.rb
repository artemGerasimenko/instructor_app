require 'rails_helper'
require 'capybara/rails'
require 'capybara/rspec'
include SessionsHelper

feature "Admin signup" do

  before(:each) do
    FactoryGirl.create(:departament, name: 'Departament_1')
  end
  it "signs asmin in with valid info" do
    visit 'signup'
    fill_in 'worker_name', :with => 'Admin'
    fill_in 'worker_email', :with => 'admin@a.a'
    fill_in 'worker_age', :with => '30'
    fill_in 'worker_status', :with => 'Admin'
    fill_in 'worker_password', :with => '123456'
    select 'Departament_1', :from => 'Отдел'
    click_button 'Создать аккаунт'

    expect(Admin.count).to eq 1

    # ask Ihor
    #expect(current_worker.id).to eq @worker.id

    click_link 'Выход'
    expect(Admin.count).to eq 1
  end
end

feature "Admin signin" do

  before(:each) do
    departament = FactoryGirl.create(:departament, name: 'Departament_1')
    FactoryGirl.create(:admin, name:'Admin', email:'admin@a.a',departament_id:departament.id,
                      password:'123456',age:22)
    FactoryGirl.create(:worker, name:'Worker', email:'worker@w.w',departament_id:departament.id,
                       password:'123456')
  end
  it "signin admin in with valid info" do
    visit '/'
    fill_in 'session_email', :with => 'admin@a.a'
    fill_in 'session_password', :with => '123456'
    click_button 'Вход'
    expect(page).to have_content('Расписание')
  end

  it "signin admin in with invalid info" do
    visit '/'
    fill_in 'session_email', :with => 'notExist'
    fill_in 'session_password', :with => 'notExist'
    click_button 'Вход'
    expect(page).to have_content('Некорректные')
  end

  scenario 'can delete worker' do
    expect(Worker.count).to eq 1
    visit '/'
    fill_in 'session_email', :with => 'admin@a.a'
    fill_in 'session_password', :with => '123456'
    click_button 'Вход'
    click_link 'Сотрудники'
    click_link 'Удалить'

    #page.driver.browser.switch_to.alert.accept()

    click_link 'Сотрудники'
    expect(Worker.count).to eq 0
  end

end
