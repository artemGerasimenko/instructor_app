require 'rails_helper'
require 'capybara/rails'
require 'capybara/rspec'


feature "Superadmin" do
  before(:each) do
    @superadmin = FactoryGirl.create(:superadmin, name:'Superadmin',email: 'superadmin@s.s',
                                     password: 'password',age:22)
  end
  scenario "signs in with valid info and work with departament" do
    visit '/'
    fill_in 'session_email', with: 'superadmin@s.s'
    fill_in 'session_password', with: 'password'
    click_button 'Вход'

    expect(Superadmin.count).to eq 1

    visit '/departaments'
    click_link 'Новый отдел'
    fill_in 'departament_name', :with => 'Departament'
    click_button 'Create Departament'
    expect(page).to have_content('Departament was successfully created')
    expect(Departament.count).to eq 1

    click_link 'Edit'
    fill_in 'departament_name', :with => 'Departament_new_name'
    click_button 'Update Departament'

    click_link 'Back'
    click_link 'Удалить'
    #page.driver.browser.switch_to.alert.accept
    visit '/departaments'
    expect(Departament.count).to eq 0


    click_link 'Выход'
    expect(Superadmin.count).to eq 1
  end

  scenario 'superadmin method show' do
    visit '/'
    fill_in 'session_email', with: 'superadmin@s.s'
    fill_in 'session_password', with: 'password'
    click_button 'Вход'
    visit '/superadmins'
    expect(page).to have_content 'Администраторы'
  end

end
