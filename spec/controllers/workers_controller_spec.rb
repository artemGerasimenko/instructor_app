require 'rails_helper'

# describe WorkersController do
#
#     let!(:departament) {Departament.create(name:'Departament')}
#     let!(:worker) {Worker.create(name: 'Worker',
#                                  email: 'w@w.com',
#                                  departament_id: departament.id,
#                                  password: 'password')}
#
#   include SessionsHelper
#
#
#     describe 'POST :create' do
#     it 'creates worker and saves it in database' do
#       expect(Worker.count).to eq 1
#       worker = Worker.first
#       expect(worker.name).to eq 'Worker'
#       expect(worker.email).to eq 'w@w.com'
#       expect(worker.departament == departament)
#       end
#     end
# end

feature 'Worker signinging' do
  before(:each) do
    departament = FactoryGirl. create(:departament,name:'Departament_1')
  end

  # does not cover
  scenario 'worker sign up' do
    visit 'signup'
    fill_in 'worker_name', :with=>'Worker'
    fill_in 'worker_email', :with=>'worker@w.w'
    fill_in 'worker_password', :with=>'123456'
    fill_in 'worker_age', :with=>20
    fill_in 'worker_phone', :with=>'+3801234567'
    select 'Departament_1', :from => 'Отдел'
    click_button 'Создать аккаунт'

    expect(Worker.count).to eq 1

    click_link 'Настройки'
    fill_in 'worker_name', :with=>'Worker new'
    fill_in 'worker_password', :with=>'123456'
    click_button 'Сохранить изменения'



    expect(Worker.count).to eq 1
    expect(Worker.first.name).to eq 'Worker new'

    # expect()
    # click_button 'noButton'
  end
end