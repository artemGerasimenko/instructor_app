require 'rails_helper'
require 'capybara/rails'
require 'capybara/rspec'
include SessionsHelper



feature "Worker signin" do
  #feature "Worker signin" , js: true do

    before(:each) do
      @worker = FactoryGirl.create(:worker, name:'Worker',email: 'worker@w.w', password: 'password',
                    departament_id:Departament.create(name:'Departament').id,age:22)
    end
    it "signs me in with valid info" do
      visit '/'
      fill_in 'session_email', with: 'worker@w.w'
      fill_in 'session_password', with: 'password'
      click_button 'Вход'

      expect(Worker.count).to eq 1

      # ask Ihor
      #expect(current_worker.id).to eq @worker.id

      click_link 'Выход'
      expect(Worker.count).to eq 1
    end
end
