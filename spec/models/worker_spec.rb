require 'rails_helper'
require 'simplecov'

describe Worker do
    it 'validates presence of column in db' do
    is_expected.to have_db_column(:name)
    is_expected.to have_db_column(:departament_id)
    is_expected.to have_db_column(:email)
    is_expected.to have_db_column(:hours)
    is_expected.to have_db_column(:phone)
    is_expected.to have_db_column(:password_digest)
    is_expected.to have_db_column(:age)
  end

  it 'validates name presence' do
    is_expected.to validate_presence_of(:name)
    is_expected.to validate_presence_of(:email)
  end

  it 'has many buckets' do
    is_expected.to have_many :bucket
  end

  it 'belongs to departament' do
    is_expected.to belong_to :departament
  end

    before {@departament = Departament.create(name:'Departament')}
    subject{@departament}

    before {@worker = Worker.new(name:"Worker", email: "worker@w.w", departament_id: @departament.id, password: "123456")}
    subject {@worker}
    it '1' do should respond_to(:name) end
    it '2' do should respond_to(:email) end
    it '3' do should respond_to(:departament_id) end
    it '4' do should respond_to(:password) end

    describe "when email format is invalid" do
      it "should be invalid" do
        addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar_baz.com foo@bar+baz.com]
        addresses.each do |invalid_address|
          @worker.email = invalid_address
          expect(@worker).not_to be_valid
        end
      end
    end


  describe "When something in not present" do
    before {@worker.email = "stamis"}
    it {should_not be_valid}
  end

  describe "when name is too long" do
    before { @worker.name = "a" * 51 }
    it { should_not be_valid }
  end

end