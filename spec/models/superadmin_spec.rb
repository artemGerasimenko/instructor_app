require 'rails_helper'
require 'simplecov'

describe Superadmin do
  it 'validates presence of column in db' do
    is_expected.to have_db_column(:name)
    is_expected.to have_db_column(:email)
    is_expected.to have_db_column(:phone)
    is_expected.to have_db_column(:password_digest)
    is_expected.to have_db_column(:age)
  end

  it 'validates name presence' do
    is_expected.to validate_presence_of(:name)
    is_expected.to validate_presence_of(:email)
    is_expected.to validate_presence_of(:password)
  end
end