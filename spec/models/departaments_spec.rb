require 'rails_helper'
require 'simplecov'

describe Departament do
  it 'validates presence of column in db' do
    is_expected.to have_db_column(:name)
    is_expected.to have_db_column(:if_schedule_on_current_week_is_approve)
  end

  it 'validates name presence' do
    is_expected.to validate_presence_of(:name)
  end

  it 'has many worker' do
    is_expected.to have_many :worker
  end

  it 'has one admin' do
    is_expected.to have_one :admin
  end

  it 'with 2 or more worker' do
    departament=Departament.create!(name:'Departament')
    worker1=Worker.create!(name:'Worker1',email:'worker1@w.w',password:'123456',
                          departament_id:departament.id,age:22)
    worker2=Worker.create!(name:'Worker2',email:'worker2@w.w',password:'123456',
                           departament_id:departament.id,age:22)
    expect(departament.reload.worker).to eq([worker1,worker2])
  end

end