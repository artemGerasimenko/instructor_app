require 'rails_helper'
require 'simplecov'
include BucketsHelper

describe Bucket do
  it 'has belong to worker' do
    is_expected.to belong_to :worker
  end

  it 'deletes oll enrolls of current_user' do
    Departament.create(name:"departament")
    Worker.create(name:"worker",email:"worker@w.w",password:"123456",
                  departament_id:Departament.first.id,age:22)
    Bucket.create(worker_id:Worker.first.id,departament_id:Departament.first.id,part_of_day:false,day:'Monday')
    Bucket.update_all(created_at: Time.now-14.day)

    expect(Bucket.count).to eq 1
    Bucket.delete_old_enrolls
    expect(Bucket.count).to eq 0
  end

  describe 'has method update_hours' do
    before(:each) do
      days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
      @departament = FactoryGirl.create(:departament, name:'Departament')
      @worker = FactoryGirl.create(:worker,name:"worker",email:"worker@w.w",password:"123456",
                                   departament_id:Departament.first.id,age:22)
      @bucket = FactoryGirl.create(:bucket,worker_id:@worker.id,departament_id:@departament.id,
                     is_chosen_on_current_week:true,part_of_day:false,day:days[Time.now.wday  ])
      @bucket.update_attribute(:created_at,Time.now-7.day)
    end

    it 'has method update_hours' do
      expect(@bucket.worker.hours).to eq 0
      Bucket.update_hours('false')
      expect(@bucket.reload.worker.hours).to eq 5
    end
  end
end