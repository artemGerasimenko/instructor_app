require 'spec_helper'
require 'rails_helper'
require 'capybara/rails'
require 'capybara/rspec'


feature 'find bucket on current week' , type: :request do
  include SessionsHelper
  before(:each) do
    @departament = FactoryGirl.create(:departament, name:'Departament_1')
    @worker = FactoryGirl.create(:worker,name:'Worker',email:'w@w.w',password:'password',
                                 departament_id:@departament.id,age:22)
    @bucket = FactoryGirl.create(:bucket,departament_id:@departament.id,worker_id:@worker.id,
                                 day:'Monday',part_of_day:false,is_chosen_on_current_week:true)
    @bucket.update_attribute(:created_at,Time.now-7.day)
  end
  scenario 'method find_bucket_on_current_week' do
    expect(find_bucket_on_current_week(@worker.id,'Monday', false, @departament.id)).to eq @bucket
  end

  # scenario 'method if_already_checked' do
  #   @bucket.update_attribute()
  #   expect(if_already_checked).to eq true
  # end

end