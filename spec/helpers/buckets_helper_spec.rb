require 'spec_helper'
require 'rails_helper'
require 'capybara/rails'
require 'capybara/rspec'
include BucketsHelper

feature 'methods:' do
  before(:each) do
    departament = FactoryGirl.create(:departament, name:'Departament_1')
    @worker = FactoryGirl.create(:worker,name:'Worker',email:'w@w.w',password:'password',
                                 departament_id:departament.id,age:22)
    @bucket = FactoryGirl.create(:bucket,departament_id:departament.id,worker_id:@worker.id,
                                 day:'Monday',part_of_day:false,is_chosen_on_current_week:true)
  end
  scenario 'testing method find_bucket' do
    expect(find_bucket(@worker.id,'Monday',false)).to eq @bucket
  end
end