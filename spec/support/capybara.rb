# frozen_string_literal: true
Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(app, browser: :chrome)
end

Capybara.server_port = 3001
Capybara.app_host = 'http://localhost:3001'

Capybara.javascript_driver = :chrome

Capybara.default_max_wait_time = 20
