FactoryGirl.define do
  factory :departament do
    sequence(:name) { |n| "departament name #{n}" }
  end
end