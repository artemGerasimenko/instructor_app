FactoryGirl.define do
  factory :bucket do
    sequence(:worker_id) { |n| "worker_id #{n}" }
    sequence(:departament_id) { |n| "departament_id #{n}" }
    sequence(:is_chosen_on_current_week) { |n=false| "is_chosen_on_current_week #{n}" }
    sequence(:part_of_day) {|n| "part_of_day #{n}"}
    sequence(:day) {|n| "day #{n}"}
  end
end