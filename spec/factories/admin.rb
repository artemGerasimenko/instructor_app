FactoryGirl.define do
  factory :admin do
    sequence(:name) { |n| "name #{n}" }
    sequence(:email) { |n| "email #{n}" }
    sequence(:password) { |n| "password #{n}" }
    sequence(:departament_id) { |n| "departament_id #{n}" }
    sequence(:age) { |n| "age #{n}" }
  end
end