FactoryGirl.define do
  factory :superadmin do
    sequence(:name) { |n| "name #{n}" }
    sequence(:email) { |n| "email #{n}" }
    sequence(:password) { |n| "password #{n}" }
  end
end