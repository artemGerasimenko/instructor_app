FactoryGirl.define do
  factory :worker do
    sequence(:name) { |n| "worker name #{n}" }
    sequence(:email) { |n| "email #{n}" }
    sequence(:departament_id) { |n| "departament_id #{n}"}
    sequence(:password) { |n| "password #{n}"}
    sequence(:age) { |n| "age #{n}"}
  end


end