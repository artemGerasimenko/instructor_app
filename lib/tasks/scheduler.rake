  desc 'This task is called by the Heroku scheduler add-on'
  task :delete_old_enrolls => :environment do
    puts 'deleting old enrolls...'
    Bucket.delete_old_enrolls
    puts 'done.'
  end

  task :make_departaments_unapproved => :environment do
    Departament.make_unapproved if Time.now.wday == 1
  end

  task :update_hours , [:part_of_day]  => :environment do |t,args|
    puts args.part_of_day
    Bucket.update_hours(args.part_of_day)
  end

